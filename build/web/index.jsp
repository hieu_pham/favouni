<%-- 
    Document   : login
    Created on : Oct 31, 2019, 4:33:45 PM
    Author     : khanhlvm98
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
        <title>Sign in</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style>
            strong{color:#49bf9d}
        </style>
</head>
<body class="is-preload">

<!-- Header -->
    <header id="header">
        <div class="inner">
            <h1>SIGN IN</h1><br>
            <form action="signin" method="POST" style="color: #49bf9d">
                Username :
                <input type="text" name="username"><br>    
                Password :
                <input type="password" name="userpass"><br>
                <a href="#">Forgot username or password?</a><br><br>
                <ul class="actions fit">
                    <li><a href="signup.jsp" class="button small fit">Sign Up</a></li>
                    <li><input type="submit" value="Login" class="button small primary fit"></li>
                </ul>
            </form>
        </div>
    </header>

<!-- Main -->
<div id="main">
    <section id="one">
            <header class="major">
                    <h3><strong>FavoUni</strong> helps you connect and join favourite activities in your university.</h3>
            </header>
            
            <section>
                <div class="box alt">
                    <div class="row gtr-50 gtr-uniform">                               
                            <div class="col-4"><span class="image fit"><img src="images/thumbs/01.jpg" alt="" /></span></div>
                            <div class="col-4"><span class="image fit"><img src="images/thumbs/02.jpg" alt="" /></span></div>
                            <div class="col-4"><span class="image fit"><img src="images/thumbs/03.jpg" alt="" /></span></div>
                            <div class="col-12"><span class="image fit"><img src="images/thumbs/cover.jpg" alt="" /></span></div>
                    </div>
                </div>
							
            </section>
    </section>
</div>	

<!-- Footer -->
    <footer id="footer">
            <div class="inner">
                    <ul class="copyright">
                        <li>&copy; 2019 Untitled</li><br>   
                        <a href="#">Terms   |</a>
                        <a href="#">Privacy    |</a>
                        <a href="#">Security    </a>
                    </ul>
            </div>
    </footer>

        <!-- Scripts -->
                <script src="assets/js/jquery.min.js"></script>
                <script src="assets/js/jquery.poptrox.min.js"></script>
                <script src="assets/js/browser.min.js"></script>
                <script src="assets/js/breakpoints.min.js"></script>
                <script src="assets/js/util.js"></script>
                <script src="assets/js/main.js"></script>

</body>
</html>