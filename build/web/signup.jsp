<%-- 
    Document   : login
    Created on : Oct 31, 2019, 4:33:45 PM
    Author     : khanhlvm98
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
        <title>Sign Up</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style>
            strong{color:#49bf9d}
        </style>
        
</head>
<body class="is-preload">

<!-- Header -->
    <header id="header">
        <div class="inner">
            <h1>SIGN UP</h1><br>
            <form action="signup" method="POST" style="color: #49bf9d">
                <input type="text" placeholder="Fullname" name="fullname"><br> 
                <input type="text" placeholder="Username" name="username"><br>
                <c:choose>
                    <c:when test="${param.failure==0}"><p><strong>Error... . username is existed!</strong></p></c:when>
                    <c:otherwise></c:otherwise>
                </c:choose>
                <input type="password" placeholder="Password" name="userpass"><br>
                <c:choose>
                    <c:when test="${param.failure==1}"><p><strong>Error... . password less 8 character</strong></p></c:when>
                    <c:otherwise></c:otherwise>
                </c:choose>
                <div class="row gtr-uniform gtr-50">
                    <div class="col-4 col-12-small">
                        Date of birth :
                    </div>
                    <div class="col-8 col-12-small">
                        <input type="date" name="dob"/>
                    </div>
                </div>
                <br>
                <div class="row gtr-uniform gtr-50">
                    <div class="col-4 col-12-small">
                        Gender :
                    </div>
                    <div class="col-4 col-12-small">
                        <input type="radio" id="demo-priority-low" name="gender" value="M" checked>
                        <label for="demo-priority-low">Male</label>
                    </div>
                    <div class="col-4 col-12-small">
                        <input type="radio" id="demo-priority-normal" name="gender" value="F">
                        <label for="demo-priority-normal">Female</label>
                    </div>
                </div>               
                <br>
                <div class="row gtr-uniform gtr-50">
                    <div class="col-6 col-12-small">
                        
                    </div>
                <ul class="actions fit">
                    <li><a href="signin" class="button small fit">Sign in</a></li>
                    <li><input type="submit" class="button primary small" value="Sign Up"></li>
                </ul>
            </form>
        </div>

    </header>

<!-- Main -->
<div id="main">
    <section id="one">
            <header class="major">
                <h3><strong>FavoUni</strong> helps you connect and join favourite activities in your university.</h3>
            </header>
            
            <section>
                <div class="box alt">
                        <div class="row gtr-50 gtr-uniform">                               
                                <div class="col-4"><span class="image fit"><img src="images/thumbs/01.jpg" alt="" /></span></div>
                                <div class="col-4"><span class="image fit"><img src="images/thumbs/02.jpg" alt="" /></span></div>
                                <div class="col-4"><span class="image fit"><img src="images/thumbs/03.jpg" alt="" /></span></div>
                                <div class="col-12"><span class="image fit"><img src="images/thumbs/cover.jpg" alt="" /></span></div>
                        </div>
                </div>
							
            </section>
    </section>
</div>	

<!-- Footer -->
    <footer id="footer">
            <div class="inner">
                    <ul class="copyright">
                        <li>&copy; 2019 Untitled</li><br>   
                        <a href="#">Terms   |</a>
                        <a href="#">Privacy    |</a>
                        <a href="#">Security    </a>
                    </ul>
            </div>
    </footer>

        <!-- Scripts -->
                <script src="assets/js/jquery.min.js"></script>
                <script src="assets/js/jquery.poptrox.min.js"></script>
                <script src="assets/js/browser.min.js"></script>
                <script src="assets/js/breakpoints.min.js"></script>
                <script src="assets/js/util.js"></script>
                <script src="assets/js/main.js"></script>

</body>
</html>