<%-- 
    Document   : index
    Created on : Oct 31, 2019, 4:26:56 PM
    Author     : khanhlvm98
--%>

<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="act" class="model.Activity" scope="session"/>
<jsp:useBean id="us" class="model.User" scope="session"/>
<!DOCTYPE HTML>

<html>
<head>
<title>Profile</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="stylesheet" href="assets/css/main.css" />
<style>
    strong{color:#3eb08f}
</style>
</head>
	<body class="is-preload">
                <!-- Header -->
			<header id="header">
                            <div class="row">
                                <div class="col-6 col-12-xsmall">
                                    <a href="#" class="image avatar"><img src="images/avatar.jpg"/></a>                                   
                                </div>
                                <div class="col-6 col-12-xsmall">
                                    <a href="home">Home</a>
                                    <br/>
                                    <a href="profile?username=${u.username}">Profile Details</a>
                                    <br/>
                                    <a href="create">Create Activities</a>
                                    <br/>
                                    <a href="manage">Manage Activities</a>
                                    <br/>
                                    <a href="logout">Logout</a>
                                </div>
                            </div>
                            <div class="row">
                                <h3><strong>${u.fullname}</strong></h3>
                            </div>                           
                            <hr/>
                            <h4>People you may know</h4>
                            <c:forEach items="${us.allUser}" var="uf">
                                <c:if test="${us.allUser!= null && us.getFriend(u.username,uf.username) != 1 && uf.username != u.username}">
                                    <ul class="actions fit">
                                        <li><a href="profile?username=${uf.username}" class="fit"> ${uf.getUser(uf.username).fullname} </a></li>
                                        <li><a href="addfriend?username=${uf.username}" class="button small fit">Add friend</a></li></ul>
                                </c:if>
                            </c:forEach>          
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					
            <section id="one">
                <header class="major">                    
                    <div class="row">
                                               
                        <c:forEach items="${us.allUser}" var="uf">
                            <c:if test="${uf.username==uff.username}">
                                <div class="col-6 col-12-xsmall">   
                                    <h2>PROFILE</h2>
                                    <a href="#" class="image" style="width: 100%"><img src="images/avatar.jpg" alt="" /></a>
                                    <h3><strong>${uf.fullname}</strong></h3>
<!--                                    <ul>                                 
                                        <li>100 friends</li>
                                        <li>10 activities</li>
                                        <li>joined 50 activities</li>

                                    </ul>                            -->
                                <div class="col-12 col-12-small">
                                                <strong>Introduction: </strong><blockquote>${uf.bio}</blockquote>                                               
                                            </div>
                                </div>
                                <div class="col-6 col-12-xsmall">
                                    <ul class="alt">
                                        <li>
                                            
                                        </li> 
                                        <li><strong>Full name: </strong>&nbsp;&nbsp; ${uf.fullname}</li>
                                        <li><strong>Date of birth: </strong>&nbsp;&nbsp; ${uf.dob}</li>
                                        <li><strong>Gender: </strong>&nbsp;&nbsp; ${uf.gender}</li>         
                                        <li><strong>Email: </strong>&nbsp;&nbsp; ${uf.email}</li>
                                        <li><strong>Phone: </strong>&nbsp;&nbsp; ${uf.phone}</li>
                                        <li><strong>Address: </strong>&nbsp;&nbsp; ${uf.userLocation}</li>
                                        
                                    </ul>
                                </div>
                            </c:if>                              
                        </c:forEach>
                            </div>                    
                    <ul class="actions fit">
                        <li><a href="edit?username=${u.username}" class="button small primary fit">Edit Profile</a></li>
                        <li><a href="changepass" class="button small primary fit">Change Password</a></li>
                        <li><a href="listfriend?username=${u.username}" class="button small primary fit">Friends</a></li>
                        <li><a href="manage" class="button small primary fit">Manage Activities</a></li>
                    </ul>                    
                </header>
            </section>
				<!-- Two -->
<section id="one">
    <div class="row">
        <h2><strong>TIMELINE</strong></h2>
    <c:forEach items="${act.allActivity}" var="a">
        <c:if test="${a.username==u.username}">
        <article class="col-12-small col-12-xsmall work-item">            
       <h2><strong>${a.actID} . ${a.actName}</strong></h2>  
        <div class="box alt">
            <div class="row gtr-50 gtr-uniform">                               
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/01.jpg" alt="" /></span></div>
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/02.jpg" alt="" /></span></div>
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/03.jpg" alt="" /></span></div>
            </div>
        </div>       
            <div class="table-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>Location</th>
                            <th>Time</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>      
                            <td>${a.actLocation}</td>
                            <td>${a.actTime}</td>
                            <td>${a.actDate}</td>                           
                            <td>${a.categories}</td>
                            <td>${a.description}</td>											
                        </tr>
                    </tbody>
                </table>
            </div>

            <ul class="actions fit">               
                <c:choose>
                    <c:when test="${act.getLike(a.actID, u.username)}"><li><a href="dislike?actID=${a.actID}" class="button primary fit">Liked</a></li></c:when>
                    <c:otherwise><li><a href="like?actID=${a.actID}" class="button fit">Like</a></li></c:otherwise>                        
                </c:choose>      
                <c:choose>
                    <c:when test="${act.getJoin(a.actID, u.username)}"><li><a href="unjoin?actID=${a.actID}" class="button primary fit">Joined</a></li></c:when>
                    <c:when test="${act.getActivity(a.actID, u.username)}"><li><a href="manage" class="button primary fit">Manage Activity</a></li></c:when>
                    <c:otherwise><li><a href="join?actID=${a.actID}" class="button fit">Join</a></li></c:otherwise>                        
                </c:choose>
                <li><a href="detail?actID=${a.actID}" class="button fit">Details</a></li>
            </ul>
            <form action="comment" method="POST">
                <div class="row">                 
                    <div class="col-8 col-12-small">
                        <textarea id="demo-message" placeholder="Enter your comment" rows="2" name="content"></textarea>
                    </div>
                    <div class="col-4 col-12-small">
                        <input type="radio" id="demo-priority-low" name="actID" value="${a.actID}" checked>
                        <input type="submit" value="Post" class="button primary"/> 
                    </div>                                                                    
                </div>  
            </form>
            <br>
            <div class="row" style=" margin: 0;padding: 5% 3% 0 0; border: 1px solid #a2a2a2; border-radius: 5px;">
                <br>
                <c:choose>
                    <c:when test="${cm.getAllComment(a.actID) != null}">
                        <c:forEach items="${cm.getAllComment(a.actID)}" var="cmt">
                            <div class="col-2 col-12-small">                                                                  
                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt=""/></a>
                            </div>  
                            <div class="col-4 col-12-small">
                                <h3><strong>${us.getUser(cmt.username).fullname}</strong></h3>
                                <ul>
                                    <li>${cmt.dateComment}</li>                                                                        
                                </ul>
                            </div>
                            <div class="col-6 col-12-small">               
                                <blockquote>${cmt.content}</blockquote>                                                                             
                            </div> 
                        </c:forEach>                       
                    </c:when>
                    <c:otherwise></c:otherwise> 
                </c:choose> 
            </div>
            <hr>
        </article>
        </c:if>
        </c:forEach>
    </div>                                         
</section>
</div>

		<!-- Footer -->
			<footer id="footer">
                            <div class="inner">
                                <ul class="copyright">
                                    <li>&copy; 2019 Untitled</li><br>   
                                    <a href="#">Terms   |</a>
                                    <a href="#">Privacy    |</a>
                                    <a href="#">Security    </a>
                                </ul>
                            </div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>


