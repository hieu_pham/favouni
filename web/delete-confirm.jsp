<%-- 
    Document   : index
    Created on : Oct 31, 2019, 4:26:56 PM
    Author     : khanhlvm98
--%>

<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="us" class="model.User" scope="session"/>
<jsp:useBean id="act" class="model.Activity" scope="session"/>
<!DOCTYPE HTML>

<html>
<head>
        <title>Delete Activity</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style>
            strong{color:#3eb08f}
        </style>
</head>
<body class="is-preload">

<!-- Header -->
<header id="header">
    <div class="row">
        <div class="col-6 col-12-xsmall">
            <a href="#" class="image avatar"><img src="images/avatar.jpg"/></a>                                   
        </div>
        <div class="col-6 col-12-xsmall">
            <a href="home">Home</a>
            <br/>
            <a href="profile?username=${u.username}">Profile Details</a>
            <br/>
            <a href="create">Create Activities</a>
            <br/>
            <a href="manage">Manage Activities</a>
            <br/>
            <a href="logout">Logout</a>
        </div>
    </div>
    <div class="row">
        <h3><strong>${u.fullname}</strong></h3>
    </div>                           
    <hr/>
    <h4>People you may know</h4>
    <c:forEach items="${us.allUser}" var="uf">
        <c:if test="${us.allUser!= null && us.getFriend(u.username,uf.username) != 1 && uf.username != u.username}">
            <ul class="actions fit">
                <li><a href="profile?username=${uf.username}" class="fit"> ${uf.getUser(uf.username).fullname} </a></li>
                <li><a href="addfriend?username=${uf.username}" class="button small fit">Add friend</a></li></ul>
        </c:if>
    </c:forEach>          
</header>

<!-- Main -->
<div id="main">
<!-- One -->
<section id="one">               
    <header class="major"> 
        <c:if test="${param.error == 0}"> 
            <h2><strong>Error...! Check again!</strong></h2>
        </c:if>
        <h2>DELETE ACTIVITY</h2>
        <form action="delete" method="post" style="color:#3eb08f">
            <div class="row gtr-uniform gtr-50">
                <div class="col-6 col-12-xsmall">
                    <input type="text" name="actName" id="demo-name" value="${ad.actName}">
                </div>
                <div class="col-3 col-12-xsmall">
                    <input type="time" name="actTime" id="demo-name" value="${ad.actTime}">
                </div>
                <div class="col-3 col-12-xsmall">
                    <input type="date" name="actDate" id="demo-name" value="${ad.actDate}">
                </div>
                <div class="col-6 col-12-xsmall">
                      <input type="text" name="actLocation" value="${ad.actLocation}"/> <br />
                </div>                        
                <div class="col-6 col-12-xsmall">
                    <select name="categories" id="demo-category">   
                        <option>${ad.categories}</option>  
                        <option value="Art">Art</option>
                        <option value="Causes">Causes</option>
                        <option value="Comedy">Comedy</option>
                        <option value="Crafts">Crafts</option>
                        <option value="Dance">Dance</option>
                        <option value="Drinks">Drinks</option>
                        <option value="Film">Film</option>
                        <option value="Fitness">Fitness</option>
                        <option value="Food">Food</option>
                        <option value="Games">Games</option>
                        <option value="Gardening">Gardening</option>
                        <option value="Health">Health</option>
                        <option value="Home">Home</option>
                        <option value="Literature">Literature</option>
                        <option value="Music">Music</option>
                        <option value="Networking">Networking</option>
                        <option value="Other">Other</option>
                        <option value="Party">Party</option>
                        <option value="Religion">Religion</option>
                        <option value="Shopping">Shopping</option>
                        <option value="Sports">Sports</option>
                        <option value="Theater">Theater</option>
                        <option value="Wellness">Wellness</option>
                    </select>
                </div>                        
                <div class="col-12">
                    <textarea name="description" id="demo-message" rows="3">${ad.description}</textarea>
                </div>                           
                <div class="col-12">
                    <ul class="actions fit">
                        <li><input type="submit" value="DELETE" class="primary"></li>
                        <li><a href="manage"class="button">CANCEL</a></li>
                    </ul>
                </div>                           
            </div>
        </form>
    </header>
</section>                                        
</div>
<!-- Footer -->
        <footer id="footer">
            <div class="inner">
                <ul class="copyright">
                    <li>&copy; 2019 Untitled</li><br>   
                    <a href="#">Terms   |</a>
                    <a href="#">Privacy    |</a>
                    <a href="#">Security    </a>
                </ul>
            </div>
        </footer>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.poptrox.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>
