<%-- 
    Document   : index
    Created on : Oct 31, 2019, 4:26:56 PM
    Author     : khanhlvm98
--%>
<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="act" class="model.Activity" scope="session"/>
<jsp:useBean id="us" class="model.User" scope="session"/>
<jsp:useBean id="cm" class="model.Comment" scope="session"/>
<!DOCTYPE HTML>

<html>
<head>
        <title>Activity Details</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style>
            strong{color:#3eb08f}
        </style>
</head>
	<body class="is-preload">

		<!-- Header -->
			<header id="header">
                            <div class="row">
                                <div class="col-6 col-12-xsmall">
                                    <a href="#" class="image avatar"><img src="images/avatar.jpg"/></a>                                   
                                </div>
                                <div class="col-6 col-12-xsmall">
                                    <a href="home">Home</a>
                                    <br/>
                                    <a href="profile?username=${u.username}">Profile Details</a>
                                    <br/>
                                    <a href="create">Create Activities</a>
                                    <br/>
                                    <a href="manage">Manage Activities</a>
                                    <br/>
                                    <a href="logout">Logout</a>
                                </div>
                            </div>
                            <div class="row">
                                <h3><strong>${u.fullname}</strong></h3>
                            </div>                           
                            <hr/>
                            <h4>People you may know</h4>
                            <c:forEach items="${us.allUser}" var="uf">
                                <c:if test="${us.allUser!= null && us.getFriend(u.username,uf.username) != 1 && uf.username != u.username}">
                                    <ul class="actions fit">
                                        <li><a href="profile?username=${uf.username}" class="fit"> ${uf.getUser(uf.username).fullname} </a></li>
                                        <li><a href="addfriend?username=${uf.username}" class="button small fit">Add friend</a></li></ul>
                                </c:if>
                            </c:forEach>          
			</header>
                        
<!-- Main -->
<div id="main">				
<!-- Two -->
<section id="one">
    <h2>ACTIVITY DETAILS</h2>
    <div class="row">    
    <c:forEach items="${act.allActivity}" var="a">
        <c:if test="${a.actID==adt.actID}">
        <article class="col-12-small col-12-xsmall work-item">            
        <h2><strong>${a.actID} . ${a.actName}</strong></h2>  
        <div class="box alt">
            <div class="row gtr-50 gtr-uniform">                               
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/01.jpg" alt="" /></span></div>
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/02.jpg" alt="" /></span></div>
                    <div class="col-4"><span class="image fit"><img src="images/thumbs/03.jpg" alt="" /></span></div>
            </div>
        </div>        
            <div class="table-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>Location</th>
                            <th>Time</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>      
                            <td>${a.actLocation}</td>
                            <td>${a.actTime}</td>
                            <td>${a.actDate}</td>                           
                            <td>${a.categories}</td>
                            <td>${a.description}</td>											
                        </tr>
                    </tbody>
                </table>
            </div>

            <ul class="actions fit">               
                <c:choose>
                    <c:when test="${act.getLike(a.actID, u.username)}"><li><a href="dislike?actID=${a.actID}" class="button primary fit">Liked</a></li></c:when>
                    <c:otherwise><li><a href="like?actID=${a.actID}" class="button fit">Like</a></li></c:otherwise>                        
                </c:choose>      
                <c:choose>
                    <c:when test="${act.getJoin(a.actID, u.username)}"><li><a href="unjoin?actID=${a.actID}" class="button primary fit">Joined</a></li></c:when>
                    <c:when test="${act.getActivity(a.actID, u.username)}"><li><a href="manage" class="button primary fit">Manage Activity</a></li></c:when>
                    <c:otherwise><li><a href="join?actID=${a.actID}" class="button fit">Join</a></li></c:otherwise>                        
                </c:choose>
                <li><input type="submit" value="BACK" onclick="javascript:history.go(-1)"></li>
            </ul>
                <c:choose>
                    <c:when test="${act.getAllJoin() != null}">
                        <div class="table-wrapper">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Member</th>
                                            <th>Join date</th>                                                                      
                                        </tr>
                                    </thead>
                        <c:forEach items="${act.getAllJoin()}" var="j">
                            <c:if test="${act.getJoin(a.actID, j.username)}">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <c:choose>                                        
                                                    <c:when test="${act.getActivity(a.actID, u.username) && u.username!=j.username}"><a href="delmember?username=${j.username}" class="button small primary">Delete</a></c:when>
                                                    <c:when test="${act.getActivity(a.actID, u.username) && u.username==j.username}"><a href="profile?username=${j.username}" class="button small primary">Author</a></c:when>
                                                    <c:otherwise><a href="profile?username=${j.username}" class="button small primary">Profile Details</a></c:otherwise>                      
                                                </c:choose>
                                            </td> 
                                            <td>${us.getUser(j.username).fullname}</td>
                                            <td>${j.joinDate}</td>                                              										
                                        </tr>
                                    </tbody>
                            </c:if>    
                        </c:forEach>
                                    </table>
                            </div>
                    </c:when>
                    <c:otherwise><p>List of member is empty</p></c:otherwise> 
                </c:choose> 
            <form action="comment" method="POST">
                <div class="row">                 
                    <div class="col-8 col-12-small">
                        <textarea id="demo-message" placeholder="Enter your comment" rows="2" name="content"></textarea>
                    </div>
                    <div class="col-4 col-12-small">
                        <input type="radio" id="demo-priority-low" name="actID" value="${a.actID}" checked>
                        <input type="submit" value="Post" class="button primary"/> 
                    </div>                                                                    
                </div>  
            </form>
            <br>
            <div class="row" style=" margin: 0;padding: 5% 3% 0 0; border: 1px solid #a2a2a2; border-radius: 5px;">
                <br>
                <c:choose>
                    <c:when test="${cm.getAllComment(a.actID) != null}">
                        <c:forEach items="${cm.getAllComment(a.actID)}" var="cmt">
                            <div class="col-2 col-12-small">                                                                  
                                <a href="#" class="image avatar"><img src="images/avatar.jpg" alt=""/></a>
                            </div>  
                            <div class="col-4 col-12-small">
                                <h3><strong>${us.getUser(cmt.username).fullname}</strong></h3>
                                <ul>
                                    <li>${cmt.dateComment}</li>                                                                        
                                </ul>
                            </div>
                            <div class="col-6 col-12-small">               
                                <blockquote>${cmt.content}</blockquote>                                                                             
                            </div> 
                        </c:forEach>                       
                    </c:when>
                    <c:otherwise></c:otherwise> 
                </c:choose> 
            </div>
            <hr>
        </article>
        </c:if>
        </c:forEach>
    </div>                                         
</section>
</div>

		<!-- Footer -->
			<footer id="footer">
                            <div class="inner">
                                <ul class="copyright">
                                    <li>&copy; 2019 Untitled</li><br>   
                                    <a href="#">Terms   |</a>
                                    <a href="#">Privacy    |</a>
                                    <a href="#">Security    </a>
                                </ul>
                            </div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
