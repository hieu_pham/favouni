<%-- 
    Document   : index
    Created on : Oct 31, 2019, 4:26:56 PM
    Author     : khanhlvm98
--%>

<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="us" class="model.User" scope="session"/>
<jsp:useBean id="act" class="model.Activity" scope="session"/>
<!DOCTYPE HTML>

<html>
<head>
        <title>Verify Account</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style>
            strong{color:#3eb08f}
        </style>
</head>
<body class="is-preload">

<!-- Header -->
<header id="header">
    <div class="row">
        <div class="col-6 col-12-xsmall">
            <a href="#" class="image avatar"><img src="images/avatar.jpg"/></a>                                   
        </div>
        <div class="col-6 col-12-xsmall">
            <a href="home">Home</a>
            <br/>
            <a href="profile?username=${u.username}">Profile Details</a>
            <br/>
            <a href="create">Create Activities</a>
            <br/>
            <a href="manage">Manage Activities</a>
            <br/>
            <a href="logout">Logout</a>
        </div>
    </div>
    <div class="row">
        <h3><strong>${u.fullname}</strong></h3>
    </div>                           
    <hr/>
    <h4>People you may know</h4>
    <c:forEach items="${us.allUser}" var="uf">
        <c:if test="${us.allUser!= null && us.getFriend(u.username,uf.username) != 1 && uf.username != u.username}">
            <ul class="actions fit">
                <li><a href="profile?username=${uf.username}" class="fit"> ${uf.getUser(uf.username).fullname} </a></li>
                <li><a href="addfriend?username=${uf.username}" class="button small fit">Add friend</a></li></ul>
        </c:if>
    </c:forEach>        
</header>

<!-- Main -->
        <div id="main">

                <!-- One -->
<section id="one">               
    <header class="major">    
        <c:if test="${param.error == 0}"> 
            <h2><strong>Error...! Check again!</strong></h2>
        </c:if>
        <h2>DELETE ACTIVITY</h2>
        <h3><strong>Verify Account, Please!</strong></h3>
        <form action="verify" method="post" style="color:#3eb08f">
            <div class="row gtr-uniform gtr-50">
                <div class="col-4 col-12-xsmall">                               
                    <label>Username: </label>
                </div>                           
                <div class="col-8 col-12-xsmall">                               
                    <input type="text" name="username" placeholder="Username">
                </div>
                <div class="col-4 col-12-xsmall">                               
                    <label>Password: </label>
                </div>
                <div class="col-8 col-12-xsmall">
                    <input type="password" name="userpass" placeholder="********">
                </div>                            
                <div class="col-12">
                    <ul class="actions">
                        <li><input type="submit" value="DELETE" class="primary"></li>
                    </ul>
                </div>                           
            </div>
        </form>
        <ul class="actions">
            <li><input type="submit" value="BACK" onclick="javascript:history.go(-1)"></li>
        </ul>
    </header>
</section>                                        
</div>
<!-- Footer -->
<footer id="footer">
    <div class="inner">
        <ul class="copyright">
            <li>&copy; 2019 Untitled</li><br>   
            <a href="#">Terms   |</a>
            <a href="#">Privacy    |</a>
            <a href="#">Security    </a>
        </ul>
    </div>
</footer>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.poptrox.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>
